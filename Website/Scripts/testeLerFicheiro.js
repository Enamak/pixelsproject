// JavaScript source code

var MAX_CLOSEST_FRIENDS = 5;
var current_top = 0;

var p = {
    "joao": {
        "keywords": ["java", "html"]
    },
    "pedro": {
        "keywords": ["java", "c"]
    },
    "filipe": {
        "keywords": ["php"]
    }
}

var keywords = {
    "java": ["joao", "pedro", "filipe"],
    "html": ["filipe"],
    "c": ["pedro"],
    "php": ["filipe"]
}

var user = "joao";
var closestUsers = getCloseFriends(p, user, keywords);
var toPrint = user + "\nPessoas com interesses mais proximos aos teus:\n";
console.log(closestUsers)
for (i = 0; i < closestUsers.length && i < current_top; i++) {
    toPrint += closestUsers[i][0] + " tem " + closestUsers[i][1] + " interesses e ou semelhancas iguais as tuas!("
    for (j = 2; j < closestUsers[i].length; j++) {
        toPrint += closestUsers[i][j];
        if (!(j === closestUsers[i].length - 1)) {
            toPrint += ", ";
        }
    }
    toPrint += ")\n";
}

console.log(toPrint);
alert(toPrint);

function getCloseFriends(users, user, keywords) {
    var close = [];
    console.log(close.length) 
    for (i = 0; i < users[user].keywords.length; i++) { // Percorre keywords do user
        var tmpKey = users[user].keywords[i]; // Keyword a ser verificada
        console.log("A verificar a keyword " + tmpKey)
        console.log(users[user].keywords)
        for (j = 0; j < keywords[tmpKey].length; j++) { // Percorre os outros users com as mesmas keywords
            var tmpUser = keywords[tmpKey][j]; // User a ser verificado
            var indice = isOnList(close, tmpUser);
            if (!(tmpUser === user)) {
                if (indice == -1) {
                    console.log("Nao existe");
                    close.push([tmpUser, 1, tmpKey])
                    if(current_top < MAX_CLOSEST_FRIENDS)
                        current_top++;
                } else {
                    console.log("Existe");
                    close[indice][1] = close[indice][1] + 1;
                    close[indice].push(tmpKey);
                    close.sort(sortFunction);
                }
            }
        }
    }

    for (i = 0; i < current_top; i++) {
        toPrint += close[i] + "\n";
    }
    return close;
}

function isOnList(userList, user) {
    for (k = 0; k < userList.length; k++) {
        if (user === userList[k][0])
            return k;
    }
    return -1;
}

function sortFunction(a, b) {
    if (a[1] === b[1]) {
        return 0;
    }
    else {
        return (a[1] < b[1]) ? 1 : -1;
    }
}
