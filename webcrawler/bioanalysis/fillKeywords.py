import json

def main():

	with open('users_keywords.json') as json_file:  
	    data = json.load(json_file)

	    kdict = {'keywords': {}}

	    for i in range(0, len(data)):
	    	for k in data[i]['keywords']:
	    		if (kdict['keywords'].has_key(k)):
	    			kdict['keywords'][k].append(data[i]['username'])
	    		else:
	    			kdict['keywords'][k] = [data[i]['username']]

		# print(kdict)

	with open('keywords_users.json', 'w') as outfile:  
	    json.dump(kdict, outfile)

if __name__ == '__main__':
	main()