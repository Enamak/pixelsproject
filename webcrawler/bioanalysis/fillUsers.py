import httplib, urllib
import json
import os
from os import walk

accessKey = 'd960cf77d1f848a5b31b15caf6eebc28'

uri = 'westeurope.api.cognitive.microsoft.com'
path = '/text/analytics/v2.0/keyPhrases'
dirPath = '../bios/'

def getKeywords (docs):
	headers = {'Ocp-Apim-Subscription-Key': accessKey}
	conn = httplib.HTTPSConnection(uri)
	body = json.dumps(docs)
	conn.request("POST", path, body, headers)
	response = conn.getresponse()
	return response.read()


def main():

	filenames = next(os.walk(dirPath))[2]
	docs = {'documents': []}

	for i in range(900, len(filenames)):
		file = open(dirPath + filenames[i], 'r')
		username = filenames[i].split('.')[0]
		content = file.read()

		docs['documents'].append({
			'id': i + 1,
			'language': 'en',
			'text': content,
			'username': username,
			'link': 'https://pixels.camp/' + username
		})

	response = getKeywords(docs)
	resp = json.loads(response)

	data = []

	for i in range(0, len(resp['documents'])):
		data.append({
			'username': docs['documents'][i]['username'],
			'keywords': resp['documents'][i]['keyPhrases'],
			'profileUrl': docs['documents'][i]['link']
			
		})

	with open('users_keywords2.json', 'w') as outfile:  
	    json.dump(data, outfile)



if __name__ == '__main__':
	main()