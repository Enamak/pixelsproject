import json

def main():

	with open('users_keywords.json') as json_file:  
	    data = json.load(json_file)

	    udict = {'users': {}}

	    for i in range(0, len(data)):
	    	udict['users'][data[i]['username']] = {
	    		'keywords': data[i]['keywords'],
	    		'profileUrl': data[i]['profileUrl']
	    	}

		# print(udict)

	with open('find_users.json', 'w') as outfile:  
	    json.dump(udict, outfile)

if __name__ == '__main__':
	main()