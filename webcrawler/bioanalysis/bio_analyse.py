import httplib, urllib
import json

accessKey = 'd960cf77d1f848a5b31b15caf6eebc28'

# uri = 'westus.api.cognitive.microsoft.com'
uri = 'westeurope.api.cognitive.microsoft.com'
path = '/text/analytics/v2.0/keyPhrases'

def getKeywords (docs):
	headers = {'Ocp-Apim-Subscription-Key': accessKey}
	conn = httplib.HTTPSConnection(uri)
	body = json.dumps(docs)
	conn.request("POST", path, body, headers)
	response = conn.getresponse()
	return response.read()


def main():

	bios = [
		"""
This is a simple text to be analysed before the test bio
		""",

		"""
Hi, my name is Gabriel!
I am a Luso-Venezuelan master student of Intelligent Systems at the University of Coimbra.

I came to Portugal 4 years ago looking to do my Informatics Engineering Degree here. I finished my Bachelor's at the Polytechnic Institute of Leiria and afterwards I moved to Coimbra to do my Master's.

After moving to Coimbra, I changed my perspective. Programming was very cool and all, but I had to put some effort into improving my soft skills. So I entered as a volunteer in two amazing organization: SPEAK and ESN. This decision was one of the best I've made. Following by another one this last summer, where I went to the Faber Venture's Summer Internship and I got to meet very interesting people, dive into the Startup world and have a real-work experience at Codacy.

Besides that, I like to walk, listen to music, discuss complex ideas and understand different perspectives.
I write my own blog at Medium in Spanish, it is about different perspectives and how they can totally change our way to interprete the world. I also tried YouTube some time ago, it was very instructive but too time-consuming.

If you happen to see me around, do not be afraid to say "Hi". :)
		"""
	]

	docs = {'documents': [
		{'id': '1', 'language': 'en', 'text': bios[0]},
		{'id': '2', 'language': 'en', 'text': bios[1]}
	]}

	# print(bios[1])

	response = getKeywords(docs)
	print(response)
	# response[0]['ola'] = 'ola'
	file = open('keywords_gabe.json', 'w')
	file.write(response)
	file.close()


if __name__ == '__main__':
	main()