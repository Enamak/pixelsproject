var webPage = require('webpage');
var fs = require('fs');
var page = webPage.create();
var folder = 'bios/'
var namesfile = 'nomes.txt';

console.log("Starting")

function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000, //< Default Max Timout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if(!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor()' timeout: NO BIO!");
                    onReady();
                    clearInterval(interval);
                    // phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 250); //< repeat check every 250ms
};

function getNames(namesfile) {
	var content = fs.read(namesfile);
	return content.split('\n')
}

function saveBIOs(urlnames, pos) {
	var url = urlnames[pos]
	var name = urlnames[pos].split('/').pop()

	page.open(url, function (status) {
		console.log("Reading public BIO from: " + url)
		console.log("Position: " + pos)
		waitFor(function() {
		    return page.evaluate(function() {
		    	var rawBio = document.getElementsByClassName("bio");
		        var bio = rawBio[0].innerHTML;
		        return (bio && bio.length > 0);
		    });
		}, function _onReady(){

			var bio = page.evaluate(function() {
		      	// var content = page.content;
				// console.log('Content: ' + content);
				var rawBio = document.getElementsByClassName("bio");
				var bio = rawBio[0].innerHTML;
			    return bio.length === 0? 'No BIO' : bio;
		    });

		    console.log("Writing BIO: " + name);
			var path = folder+name+'.txt';
			fs.write(path, bio, 'w');

			console.log("Saved!")
			console.log("NEXT!")
			if (pos < urlnames.length) {
				saveBIOs(urlnames, pos + 1)
			}
			else {
				phantom.exit()
			}
		});
	});
}

console.log("Working ...")

var urlnames = getNames(namesfile)
saveBIOs(urlnames, 0)
